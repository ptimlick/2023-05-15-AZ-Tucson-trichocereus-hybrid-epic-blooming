Creates a time lapse video from a series of jpeg images. Uses ffmpeg on a Linux pc.
A set of images and the output video is included.
The images were recorded on a Samsung S9 running open-camera.  

https://play.google.com/store/apps/details?id=net.sourceforge.opencamera&hl=en_US&gl=US

open-camera was set to take a picture every 5 minutes and was manually stopped after about 12 hours. 
